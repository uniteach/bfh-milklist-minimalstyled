package bfh.domain;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;


import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.bfh.milklist.Application;
import ch.bfh.milklist.domain.Item;
import ch.bfh.milklist.domain.ItemRepository;

@TestPropertySource("classpath:application-test.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=Application.class)
public class ItemRepositoryTest {

	@Autowired
	ItemRepository itemRepo;
	
	@Autowired
	private JdbcTemplate jdbc;
	
	//Werte für die Testinitialisierung
	static final String ORIGINAL_NAME_1 = "ORIGINAL NAME 1";
	static final int ORIGINAL_QTY_1 = 55;
	long ORIGINAL_ID;
	
	/**
	 * Fügt ein paar Daten direkt via JDBC in die DB ein.
	 */
	@Before
	public void setUp(){
		jdbc.update("insert into items (name, quantity) values (?,?)",ORIGINAL_NAME_1,ORIGINAL_QTY_1);
		ORIGINAL_ID = jdbc.queryForObject("select IDENTITY();", Long.class);
	}
	
	
	/**
	 *Löscht die Items Tabelle
	 */
	@After
	public void tearDown()
	{
		jdbc.update("delete from items");
	}
	
	/**
	 * Insert eines Items, danach alle auslesen und prüfen ob das Objekt mit der zurückgelieferten ID auch tatsächlich die richtigen Werte hat.
	 */
	@Test
	public void testInsert() {
		long id = itemRepo.insert(new Item(0,"testInsert",5));
		Collection<Item> itemList = itemRepo.getAll();
		for(Item i:itemList)
		{
			if(i.getId()==id)
			{
				assertThat(i.getQuantity(),is(5));
			}
		}
	}
	
	/**
	 * Update eines Objekts und danach prüfen ob es die richtigen Werte hat
	 */
	@Test
	public void testUpdate() {
		
		final String NEW_NAME = "testUpdate2";
		final int NEW_QTY = ORIGINAL_QTY_1 + 66;
		
		
		Item newOne = itemRepo.getById(ORIGINAL_ID);
		newOne.setName(NEW_NAME);
		newOne.setQuantity(NEW_QTY);
		itemRepo.save(newOne);
		
		Item updatedOne = itemRepo.getById(ORIGINAL_ID);
		assertThat(updatedOne.getName(), is(NEW_NAME));
		assertThat(updatedOne.getQuantity(), is(NEW_QTY));
	}
	
	

}
