package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import ch.bfh.milklist.web.ListManagementRootController;


/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */


@Component() 
public class ItemRepository {
	
	private final Logger logger = LoggerFactory.getLogger(ItemRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;

	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Item> getAll()  {
		logger.trace("getAll");
		return jdbc.query("select id, name, quantity from items", new ItemMapper());
	}

	/**
	 * Liefert alle Items mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Item> getByName(String name) {
		logger.trace("getByName " + name);
		return jdbc.query("select id, name, quantity from items where name=?",new Object [] {name}, new ItemMapper());
			
	}

	/**
	 * Liefert das Item mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Item getById(long id) {
		logger.trace("getById " + id);
		return jdbc.queryForObject("select id, name, quantity from items where id=?",new Object [] {id}, new ItemMapper());
	}

	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Item i) {
		logger.trace("save " + i);
		jdbc.update("update items set name=?, quantity=? where id=?",i.getName(),i.getQuantity(),i.getId());
	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		logger.trace("delete " + id);
		jdbc.update("delete from items where id=?",id);

	}

	/**
	 * Speichert das angegebene Item inder DB. INSERT.
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public long insert(Item i) {
		
		logger.trace("insert " + i);
		jdbc.update("insert into items (name, quantity) values (?,?)",i.getName(),i.getQuantity());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		return id;
	}
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Brigen
	 *
	 */
	private class ItemMapper implements RowMapper<Item>
	{
		@Override
		public Item mapRow(ResultSet rs, int rownum) throws SQLException {
			Item i = new Item(rs.getLong("id"),rs.getString("name"),rs.getInt("quantity"));
			//logger.trace("Returning Item " + i);
			return i;
		}
		
	}

}
