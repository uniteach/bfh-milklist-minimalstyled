package ch.bfh.milklist.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ch.bfh.milklist.domain.Item;
import ch.bfh.milklist.domain.ItemRepository;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */
@Controller
public class ItemController {
	
	private final Logger logger = LoggerFactory.getLogger(ItemController.class);
	
	//Autowired --> Wird durch das Springframework initialisiert (nach Typ oder Variablenname)
	@Autowired
	private ItemRepository itemRepo;
	
	//Für jede Klasse die von diesem Controller aus Formulardaten gefüllt werden muss, muss eine Factorymethode erstellt werden
	//Das erzeugte Objekt wird dann abgefüllt
	//ModelAttribute --> Muss auf den im HTML-Formular (bzw. -Template) verwendeten Namen lauten
	@ModelAttribute("itemDetail")
	public Item createItem()
	{
		return new Item();
	}
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @param model (Java --> Template) Hier werden die Werte vom Controller an das Template übergeben und dann durch die Tyhmeleaf-Engine zu HTML umgewandelt
	 * @param id    (WWW --> Java) Wenn id = 0 dann zeigt der Submitbutton auf "INSERT" ansonsten auf "UPDATE"
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	@RequestMapping(method=GET, value="/item")
	String editItem(Model model, @RequestParam(required=false) Long id)
	{
		if(null == id)
		{
			logger.trace("GET /item für INSERT mit id " + id);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.addAttribute("postAction", "/item/new");
		}
		else
		{
			logger.trace("GET /item für UPDATE mit id " + id);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.addAttribute("postAction", "/item/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Item i = itemRepo.getById(id);
			model.addAttribute("itemDetail", i);
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return "itemDetailTemplate";
	}
	
	
	/**
	 * Schreibt das geänderte Item zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/item) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /item/update
	 * 
	 * @param parameters (Java --> Redirect) Hier werden die Parameter für den Redirect nach einem erfolgreichen Update übergeben: die id des bearbeiteten Items 
	 * @param itemDetail (WWW --> Java) Hier kommen die vom Formular submitteten daten zurück.
	 * @return redirect nach /item: via Browser wird /item aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	@RequestMapping(method=POST, value="/item/update")
	String updateItem(RedirectAttributes parameters, @ModelAttribute("itemDetail") Item itemDetail)
	{
		logger.trace("POST /item/update mit itemDetail " + itemDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		itemRepo.save(itemDetail);
		parameters.addAttribute("id", itemDetail.getId());
		
		return "redirect:/item";
	}
	
	/**
	 * Löscht das Item mit der übergebenen id in der Datenbank
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /item/delete (besser wäre POST)
	 * 
	 * @param parameters (Java --> Redirect) Hier nicht verwendet da die Liste ohne Parameter aufgerufen wird
	 * @param id (WWW --> Java) Id des Items welches gelöscht werden soll.
	 * @return Redirect zurück zur Liste
	 */
	@RequestMapping(path="/item/delete")
	String deleteItem(RedirectAttributes parameters, @RequestParam Long id)
	{
		logger.trace("GET /item/delete mit id " + id);
		
		itemRepo.delete(id);
		
		return "redirect:/";
	}
	
	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @param parameters (Java --> Redirect) Hier wird die ID als Parameter nach einem erfolgreichen Update im Feld "id" angehängt
	 * @param itemDetail (WWW --> Java) Das zu erstellende Item wird hier vom Formular zurückgegeben
	 * @return Redirect zurück zur Detailmaske
	 */
	@RequestMapping(method=POST, path="/item/new")
	String newItem(RedirectAttributes parameters, Item itemDetail)
	{
		logger.trace("POST /item/new mit itemDetail " + itemDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = itemRepo.insert(itemDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item&id=432932
		parameters.addAttribute("id", id);
		return "redirect:/item";
	}

}
